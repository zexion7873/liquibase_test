--liquibase formatted sql

--changeset kevin:1
--comment: create person table
create table person (
    id int primary key,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)

--changeset kevin:2
--comment: create company table
create table company (
    id int primary key,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)

--changeset other.dev:3
--comment: add country column to  person table 
alter table person add column country varchar(2)

